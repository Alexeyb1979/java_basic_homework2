package ua.com.dan_it.fs;

import java.lang.reflect.Array;
import java.util.Random;
import java.util.Scanner;

public class HomeWork2Shooter {
    public static void main(String[] args) {

        String[][] field = new String[6][6];
        int indHor = 0;
        int indVer = 1;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (i == 0) {
                    field[i][j] = Integer.toString(indHor++) + "|";
                } else if (i != 0 && j == 0) {
                    field[i][j] = Integer.toString(indVer++) + "|";
                } else {
                    field[i][j] = "+";
                }
            }
        }

        System.out.println("All set. Get ready to rumble!");
        Random random = new Random();
        int HorIndex = random.nextInt(5) + 1;
        int VertIndex = random.nextInt(5) + 1;
        field[HorIndex][VertIndex] = "*";

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + "\t");
            }
            System.out.println();
        }

        while (field[HorIndex][VertIndex].equals("*")) {
            Scanner scanner = new Scanner(System.in);
            int shooterLine = 0;
            System.out.println("Enter shooter line:");
            boolean result = false;
            while (result == false) {
              try {
                    shooterLine = Integer.parseInt(scanner.next());
                    if (shooterLine > field.length || shooterLine < 1){
                        System.out.println("Enter shooter line number from 1 to 5 again:");
                    } else {
                        result = true;
                    }
                } catch (Exception e) {
                  System.out.println("You entered not a number.Enter shooter line number from 1 to 5 again:");
                }
            }

            System.out.println("Enter shooter column:");
            result = false;
            int shooterColumn = 0;

            while (result == false) {
                try {
                    shooterColumn = Integer.parseInt(scanner.next());
                    if (shooterColumn > field.length || shooterColumn < 1){
                        System.out.println("Enter shooter column number from 1 to 5 again:");
                    } else {
                        result = true;
                    }
                } catch (Exception e) {
                    System.out.println("You entered not a number.Enter shooter column number from 1 to 5 again:");
                }
            }

            if (field[HorIndex][VertIndex] != field[shooterLine][shooterColumn]) {
                field[shooterLine][shooterColumn] = "-";
            } else {
                field[HorIndex][VertIndex] = "X";
            }
        }

        System.out.println("You have won!");
        for (
                int i = 0;
                i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
